#![allow(incomplete_features)]
#![feature(generic_const_exprs)]
#![feature(let_else)]
#![feature(once_cell)]
#![feature(option_result_contains)]
#![feature(precise_pointer_size_matching)]

mod dict;
mod game;
mod ui;
mod util;

fn main() {
    std::panic::set_hook(Box::new(console_error_panic_hook::hook));

    ui::render();
}
