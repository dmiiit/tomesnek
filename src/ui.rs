use std::cmp::Reverse;

use enumset::EnumSet;
use fastrand::Rng;
use ordered_float::NotNan;
use seed::{prelude::*, *};

use crate::game::{
    word_score, Grid, Tile, TileBoost, TileCoords, TileLetter, TileValue, Value, MIN_VOWELS,
};

const MAX_TOP_WORDS: usize = 20;

struct Model {
    rng: Rng,
    grid: Grid<4, 4>,
    current_word: Vec<TileCoords>,
    current_word_score: Option<(String, Value)>,
    most_recent_word: Option<String>,
    top_words: Vec<(String, Value)>,
}

#[derive(Debug, Clone, Copy)]
enum Msg {
    UseTile(TileCoords),
    RemoveTile(usize),
    ScrambleGrid,
    SubmitWord,
}

pub fn render() { App::start("app", init, update, view); }

fn init(_: Url, _: &mut impl Orders<Msg>) -> Model {
    let rng = fastrand::Rng::new();
    let grid = Grid::random(&rng);
    let current_word = Vec::with_capacity(grid.size());
    let top_words = Vec::with_capacity(MAX_TOP_WORDS + 1);

    Model { rng, grid, current_word, current_word_score: None, most_recent_word: None, top_words }
}

fn update(msg: Msg, model: &mut Model, _: &mut impl Orders<Msg>) {
    match msg {
        Msg::UseTile(coords) => {
            model.current_word.push(coords);
            let tiles = model
                .current_word
                .iter()
                .map(|&coords| model.grid.tile(coords))
                .copied()
                .collect::<Vec<_>>();
            model.current_word_score = word_score(&tiles);
        }
        Msg::RemoveTile(idx) => {
            model.current_word.truncate(idx);
            let tiles = model
                .current_word
                .iter()
                .map(|&coords| model.grid.tile(coords))
                .copied()
                .collect::<Vec<_>>();
            model.current_word_score = word_score(&tiles);
        }
        Msg::ScrambleGrid => {
            model.current_word.clear();
            model.current_word_score = None;
            model.grid = Grid::random(&model.rng);
        }
        Msg::SubmitWord => {
            let (word, score) = model.current_word_score.take().unwrap();
            // Avoid duplicate words in top list.
            if !model.top_words.iter().any(|(w, _)| w == &word) {
                model.top_words.push((word.clone(), score));
                model.top_words.sort_by_key(|(_, score)| Reverse(NotNan::new(*score).unwrap()));
                model.top_words.truncate(MAX_TOP_WORDS);
            }
            model.most_recent_word = Some(word);

            let mut boosts_used = {
                let mut set = EnumSet::new();
                for &coords in &model.current_word {
                    let tile = model.grid.tile(coords);
                    if let TileValue::Letter { boost, .. } = tile.value {
                        boost.iter().for_each(|b| {
                            set.insert(*b);
                        });
                    }
                }
                set
            };

            //TODO: also consider overkill margin when doling out boosts.
            let mut boost = match score {
                n if n < 5. => None,
                n if n < 6. => Some(TileBoost::Amethyst),
                n if n < 7. => Some(TileBoost::Emerald),
                n if n < 8. => Some(TileBoost::Sapphire),
                n if n < 9. => Some(TileBoost::Garnet),
                n if n < 10. => Some(TileBoost::Ruby),
                n if n < 11. => Some(TileBoost::Crystal),
                _ => Some(TileBoost::Diamond),
            };

            let mut num_vowels = model.grid.num_vowels();
            for &coords in &model.current_word {
                // Adjust the vowel count if a vowel tile is about to get removed.
                if model.grid.tile(coords).value.is_vowel() {
                    num_vowels -= 1;
                }

                let value = if boosts_used.len() >= 3 {
                    // Reset the boost count to avoid multiple wildcards.
                    boosts_used.clear();
                    TileValue::Wildcard
                } else {
                    //TODO: spawn boost and wildcard tiles when conditions are met.
                    let letter = if num_vowels < MIN_VOWELS {
                        num_vowels += 1;
                        TileLetter::random_vowel(&model.rng)
                    } else {
                        TileLetter::random(&model.rng)
                    };
                    TileValue::Letter { letter, boost }
                };
                boost.take();
                let tile = Tile { value, debuff: None };
                *model.grid.tile_mut(coords) = tile;
            }

            model.current_word.clear();
        }
    }
}

fn view(model: &Model) -> Node<Msg> {
    let word_tiles = model.current_word.iter().map(|&coords| model.grid.tile(coords));
    let is_valid_word = model.current_word_score.is_some();

    div![
        div![
            id!("current-word"),
            C!(IF!(is_valid_word => "valid")),
            word_tiles.enumerate().map(|(idx, tile)| view_tile(tile, Some(Msg::RemoveTile(idx))))
        ],
        table![
            id!("grid"),
            model.grid.rows().iter().enumerate().map(|(x, row)| tr![row.iter().enumerate().map(
                |(y, tile)| td![view_tile(
                    tile,
                    if !model.current_word.contains(&(x, y)) {
                        Some(Msg::UseTile((x, y)))
                    } else {
                        None
                    }
                )]
            )]),
        ],
        nav![
            id!("controls"),
            button!("🔀 Scramble", ev(Ev::Click, |_| Msg::ScrambleGrid)),
            button!(
                "✅ Submit",
                ev(Ev::Click, |_| Msg::SubmitWord),
                IF!(!is_valid_word => attrs! { At::Disabled => true }),
            ),
        ],
        ol![
            id!("top-words"),
            model.top_words.iter().map(|(word, score)| li!(
                C!(IF!(model.most_recent_word.contains(word) => "most-recent")),
                format!("{:.1} — {}", score, word)
            )),
        ],
    ]
}

fn view_tile(tile: &Tile, on_click: Option<Msg>) -> Node<Msg> {
    let rank = tile.value.rank();
    let boost = match tile.value {
        TileValue::Letter { boost, .. } => boost,
        TileValue::Wildcard => None,
    };
    div![
        C!(
            "tile",
            rank.as_str(),
            boost.map(|b| b.as_str()).unwrap_or("plain"),
            IF!(on_click.is_none() => "disabled"),
            IF!(tile.value == TileValue::Wildcard => "wildcard"),
        ),
        tile.value.as_str(),
        ev(Ev::Click, move |_| on_click),
    ]
}
