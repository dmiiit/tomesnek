use fastrand::Rng;

/// Pick a random value from a slice.
pub fn pick_random<'a, T>(rng: &Rng, values: &'a [T]) -> Option<&'a T> {
    if values.is_empty() {
        return None;
    }

    Some(&values[rng.usize(0..values.len())])
}

/// Randomly pick a value from an iterator. The probability of a value being
/// picked depends on its weight. Higher weight means proportionally higher
/// chances of getting picked, so an iterator over two items with weights of
/// `0.5` and `1.5` will yield the second item in ~75% of cases.
///
/// Uses reservoir sampling, so time complexity is `O(n)` (where `n` is number
/// of values in the iterator) and space complexity is `O(1)`.
///
/// Returns `None` if the iterator is empty.
pub fn pick_weighted<T, I, W, F>(rng: &Rng, values: I, weight: F) -> Option<T>
where
    I: Iterator<Item = T>,
    W: Into<f64>,
    F: Fn(&T) -> W,
{
    let mut result = None;
    let mut total_weight = 0.;

    for value in values {
        let weight = weight(&value).into();
        total_weight += weight;
        if rng.f64() <= weight / total_weight {
            result = Some(value);
        }
    }

    result
}

// Hoo boy, testing RNG stuff.
#[cfg(test)]
mod tests {
    use fastrand::Rng;

    use super::pick_weighted;

    #[test]
    fn test_pick_weighted() {
        const ARY: &[char] = &['a', 'b', 'c', 'd', 'e', 'f'];
        let rng = Rng::with_seed(0xF00D_CAFE);

        assert_eq!(
            Some(&'c'),
            pick_weighted(&rng, ARY.iter(), |&ch| if *ch == 'c' { 999. } else { 0.01 })
        );
    }
}
