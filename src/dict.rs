use std::sync::LazyLock;

use one_sixth::WordSet;

pub static DICT: LazyLock<WordSet<&[u8]>> = LazyLock::new(|| {
    const DICT_FST: &[u8] = include_bytes!("../dict.fst");
    WordSet::load(DICT_FST).expect("failed to load dictionary")
});
