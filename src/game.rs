use enumset::EnumSetType;
use fastrand::Rng;

use crate::{dict::DICT, util::pick_weighted};

pub const MIN_VOWELS: usize = 3;
pub const BRONZE_TILE_WEIGHT: f64 = 13.;
pub const SILVER_TILE_WEIGHT: f64 = 6.;
pub const GOLD_TILE_WEIGHT: f64 = 1.;

pub type TileCoords = (usize, usize);
pub type Value = f32;

pub struct Grid<const X: usize, const Y: usize> {
    tiles: [[Tile; Y]; X],
}

impl<const X: usize, const Y: usize> Grid<X, Y> {
    pub const SIZE: usize = { X * Y };

    pub const fn size(&self) -> usize { Self::SIZE }

    pub fn random(rng: &Rng) -> Self {
        const PLACEHOLDER: Tile = Tile { value: TileValue::Wildcard, debuff: None };
        let mut tiles = [[PLACEHOLDER; { Y }]; { X }];

        let mut tiles_set = 0;
        let mut num_vowels = 0;

        for row in &mut tiles {
            for tile in row {
                let tiles_remaining = Self::SIZE - tiles_set;
                let letter = if num_vowels + tiles_remaining <= MIN_VOWELS {
                    TileLetter::random_vowel(rng)
                } else {
                    TileLetter::random(rng)
                };
                let value = TileValue::Letter { letter, boost: None };
                *tile = Tile { value, debuff: None };
                tiles_set += 1;
                if letter.is_vowel() {
                    num_vowels += 1;
                }
            }
        }

        Self { tiles }
    }

    pub fn rows(&self) -> &[[Tile; Y]] { &self.tiles }

    pub fn tile(&self, (x, y): TileCoords) -> &Tile { &self.tiles[x][y] }

    pub fn tile_mut(&mut self, (x, y): TileCoords) -> &mut Tile { &mut self.tiles[x][y] }

    pub fn num_vowels(&self) -> usize {
        let mut total = 0;
        for row in &self.tiles {
            for tile in row {
                if tile.value.is_vowel() {
                    total += 1;
                }
            }
        }
        total
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Tile {
    pub value: TileValue,
    pub debuff: Option<TileDebuff>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TileValue {
    Letter { letter: TileLetter, boost: Option<TileBoost> },
    Wildcard,
}

impl TileValue {
    pub const fn is_vowel(&self) -> bool {
        match *self {
            TileValue::Letter { letter, .. } => letter.is_vowel(),
            TileValue::Wildcard => true,
        }
    }

    pub const fn rank(&self) -> TileRank {
        match *self {
            Self::Letter { letter, .. } => letter.rank(),
            Self::Wildcard => TileRank::Bronze,
        }
    }

    pub fn value(&self) -> Value {
        match *self {
            Self::Letter { letter, boost } => {
                letter.value() * boost.map(|b| b.multiplier()).unwrap_or(1.)
            }
            Self::Wildcard => 1.,
        }
    }

    pub const fn as_str(&self) -> &'static str {
        match *self {
            Self::Letter { letter, .. } => letter.as_str(),
            Self::Wildcard => "?",
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
#[rustfmt::skip]
pub enum TileLetter {
    A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Qu, R, S, T, U, V, W, X, Y, Z,
}

impl TileLetter {
    const ALL: [TileLetter; 26] = {
        use TileLetter::*;
        [A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Qu, R, S, T, U, V, W, X, Y, Z]
    };

    const VOWELS: [TileLetter; 6] = {
        use TileLetter::*;
        [A, E, I, O, U, Y]
    };

    pub const fn is_vowel(&self) -> bool {
        use TileLetter::*;
        matches!(*self, A | E | I | O | U | Y)
    }

    pub const fn rank(&self) -> TileRank {
        use TileLetter::*;
        match *self {
            A | D | E | G | I | L | N | O | R | S | T | U => TileRank::Bronze,
            B | C | F | H | M | P | V | W | Y => TileRank::Silver,
            J | K | Qu | X | Z => TileRank::Gold,
        }
    }

    pub const fn value(&self) -> Value {
        use TileLetter::*;
        match *self {
            Qu => 2.75,
            X | Z => 2.,
            J | K => 1.75,
            V | W | Y => 1.5,
            B | C | F | H | M | P => 1.25,
            A | D | E | G | I | L | N | O | R | S | T | U => 1.,
        }
    }

    pub const fn as_str(&self) -> &'static str {
        use TileLetter::*;
        match *self {
            A => "a",
            B => "b",
            C => "c",
            D => "d",
            E => "e",
            F => "f",
            G => "g",
            H => "h",
            I => "i",
            J => "j",
            K => "k",
            L => "l",
            M => "m",
            N => "n",
            O => "o",
            P => "p",
            Qu => "qu",
            R => "r",
            S => "s",
            T => "t",
            U => "u",
            V => "v",
            W => "w",
            X => "x",
            Y => "y",
            Z => "z",
        }
    }

    pub fn random(rng: &Rng) -> Self {
        pick_weighted(rng, Self::ALL.iter(), |letter| match letter.rank() {
            TileRank::Bronze => BRONZE_TILE_WEIGHT,
            TileRank::Silver => SILVER_TILE_WEIGHT,
            TileRank::Gold => GOLD_TILE_WEIGHT,
        })
        .copied()
        .unwrap()
    }

    pub fn random_vowel(rng: &Rng) -> Self {
        pick_weighted(rng, Self::VOWELS.iter(), |letter| match letter.rank() {
            TileRank::Bronze => BRONZE_TILE_WEIGHT,
            TileRank::Silver => SILVER_TILE_WEIGHT,
            TileRank::Gold => GOLD_TILE_WEIGHT,
        })
        .copied()
        .unwrap()
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum TileRank {
    Bronze,
    Silver,
    Gold,
}

impl TileRank {
    pub const fn as_str(&self) -> &'static str {
        match *self {
            Self::Bronze => "bronze",
            Self::Silver => "silver",
            Self::Gold => "gold",
        }
    }
}

#[derive(Debug, EnumSetType)]
pub enum TileBoost {
    Amethyst,
    Emerald,
    Sapphire,
    Garnet,
    Ruby,
    Crystal,
    Diamond,
}

impl TileBoost {
    pub const fn multiplier(&self) -> Value {
        match *self {
            Self::Amethyst => 1.15,
            Self::Emerald => 1.2,
            Self::Sapphire => 1.25,
            Self::Garnet => 1.3,
            Self::Ruby => 1.35,
            Self::Crystal => 1.5,
            Self::Diamond => 2.,
        }
    }

    pub const fn as_str(&self) -> &'static str {
        match *self {
            Self::Amethyst => "amethyst",
            Self::Emerald => "emerald",
            Self::Sapphire => "sapphire",
            Self::Garnet => "garnet",
            Self::Ruby => "ruby",
            Self::Crystal => "crystal",
            Self::Diamond => "diamond",
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TileDebuff {
    /// Do damage to player instead, when used.
    Cursed,
    /// Burn other tiles.
    Firey,
    /// Cannot be used until turn count runs out.
    Locked { turns_left: u8 },
    /// Do no damage; can infect other tiles.
    Plagued,
    /// Do no damage.
    Smashed,
    /// Do no damage; warp into hard letters.
    Warped,
}

fn word_len_value(len: usize) -> Value {
    match len {
        0 | 1 | 2 => 0.,
        3 => 0.5,
        n => {
            let mut total = 0.5;
            let mut delta = 0.25;
            for i in 4..=n {
                total += delta;
                if i % 2 == 1 {
                    delta += 0.25;
                }
            }
            total
        }
    }
}

fn word_tile_value(word: impl Iterator<Item = Tile>) -> Value {
    let mut total = 0.;
    for tile in word {
        total += tile.value.value();
    }
    total
}

enum SearchPattern {
    Literal(String),
    Regex(String),
}

fn word_pattern(word: impl Iterator<Item = TileValue>) -> SearchPattern {
    // Default to 16 as that's the max word size for a 4x4 grid, and add one extra
    // byte for Qu.
    let mut pat = String::with_capacity(word.size_hint().1.unwrap_or(16) + 1);
    let mut is_regex = false;
    for tile in word {
        if let TileValue::Letter { letter, .. } = tile {
            pat.push_str(letter.as_str());
        } else {
            pat.push('.');
            is_regex = true;
        }
    }

    if is_regex { SearchPattern::Regex(pat) } else { SearchPattern::Literal(pat) }
}

/// Returns `Some(value)` for valid words and `None` otherwise.
pub fn word_score(tiles: &[Tile]) -> Option<(String, Value)> {
    if tiles.len() < 3 {
        return None;
    }

    if tiles.iter().any(|tile| matches!(tile.debuff, Some(TileDebuff::Locked { .. }))) {
        return None;
    }

    let tile_values = tiles.iter().map(|tile| tile.value);
    match word_pattern(tile_values) {
        SearchPattern::Literal(str) => DICT.contains(&str).then(|| {
            let score = word_len_value(tiles.len()) + word_tile_value(tiles.iter().copied());
            (str, score)
        }),
        SearchPattern::Regex(pat) => {
            if let Some(word) = DICT.find_pattern(&pat).unwrap().next() {
                let score = word_len_value(word.len()) + word_tile_value(tiles.iter().copied());
                Some((word, score))
            } else {
                None
            }
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn word_len_value() {
        use super::word_len_value;

        assert_eq!(word_len_value(3), 0.5);
        assert_eq!(word_len_value(4), 0.75);
        assert_eq!(word_len_value(5), 1.);
        assert_eq!(word_len_value(6), 1.5);
        assert_eq!(word_len_value(7), 2.);
        assert_eq!(word_len_value(8), 2.75);
        assert_eq!(word_len_value(9), 3.5);
        assert_eq!(word_len_value(10), 4.5);
        assert_eq!(word_len_value(11), 5.5);
        assert_eq!(word_len_value(12), 6.75);
        assert_eq!(word_len_value(13), 8.);
        assert_eq!(word_len_value(14), 9.5);
        assert_eq!(word_len_value(15), 11.);
        // 16-letter words are 13 points in the original game, but 12.75
        // according to our formula. Since we theoretically support grid sizes
        // above 4x4, it'd be nice to have a closed form solution for word
        // scores instead of a lookup table. So in this case we punt and live
        // with the 0.25-point difference.
        // assert_eq!(word_len_value(16), 13.);
    }
}
