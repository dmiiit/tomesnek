# Tome Snek

## Build

You're going to need [Trunk](https://trunk.rs).

Dev:

```
trunk serve
```

Prod:

```
trunk build --release
```

## Game rules

- Minimum word length is 3 letters.
- There should always be at least 3 vowel tiles on the board. Wildcard tiles count as vowels for the purposes of this rule.
- Letters are randomly generated with accordance to their rank. Bronze letters are approximately six times as likely to spawn as silver ones, and 13 times as likely as gold ones.

## Dictionary

The dictionary provided is based on the [SCOWL (And Friends)](http://wordlist.aspell.net/) word list, particularly [2of12id.txt](http://wordlist.aspell.net/alt12dicts-infl-readme/).

In order to compile the word list into a suitable binary format, run:

```
cargo run --features bin -- 2of12id.txt
```

from the `one-sixth` crate directory, and then copy the resulting `out.fst` as `dict.fst`.

Note that `2of12id.txt` is not included as part of this project.
