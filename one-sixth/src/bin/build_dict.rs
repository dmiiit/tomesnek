#![feature(let_else)]

use std::{
    collections::HashSet,
    error::Error,
    fs::{read_to_string, File},
    io::BufWriter,
    path::PathBuf,
    str::FromStr,
};

use argh::FromArgs;
use one_sixth::WordSetConfig;

fn main() {
    let args: Args = argh::from_env();

    if let Err(err) = run(&args) {
        eprintln!("Failed to build dictionary: {}", err);
        std::process::exit(1);
    }
}

enum DictFormat {
    Map,
    Set,
}

impl FromStr for DictFormat {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.eq_ignore_ascii_case("map") {
            Ok(Self::Map)
        } else if s.eq_ignore_ascii_case("set") {
            Ok(Self::Set)
        } else {
            Err(format!("invalid output format: {}", s))
        }
    }
}

/// Build a compressed dictionary from a words file in the 2of12id.txt format.
#[derive(FromArgs)]
struct Args {
    /// output format
    #[argh(option, short = 'f', default = "DictFormat::Set")]
    output_format: DictFormat,
    /// minimum length of included words
    #[argh(option, short = 'l', default = "3")]
    min_word_length: usize,
    /// skip obscure words
    #[argh(switch, short = 'r')]
    skip_rare_words: bool,
    /// skip archaic words
    #[argh(switch, short = 'a')]
    skip_archaic_words: bool,
    /// skip neologisms
    #[argh(switch, short = 'n')]
    skip_neologisms: bool,
    /// path to words file in the 2of12id.txt format
    #[argh(positional)]
    words_file: PathBuf,
    /// path to list of words to exclude; one word per line
    #[argh(positional)]
    excluded_words_file: Option<PathBuf>,
}

fn run(args: &Args) -> Result<(), Box<dyn Error>> {
    let excluded_words = args
        .excluded_words_file
        .as_ref()
        .map(|path| {
            std::fs::read_to_string(path)
                .map(|s| s.lines().map(ToOwned::to_owned).collect::<HashSet<_>>())
        })
        .transpose()?
        .unwrap_or_default();

    let config = WordSetConfig {
        min_word_length: args.min_word_length,
        include_archaic_words: !args.skip_archaic_words,
        include_rare_words: !args.skip_rare_words,
        include_neologisms: !args.skip_neologisms,
        excluded_words,
    };

    let source = read_to_string(&args.words_file)?;

    let out_file = File::create("out.fst")?;
    let mut buf = BufWriter::new(out_file);

    match args.output_format {
        DictFormat::Map => {
            let dict = one_sixth::parser::parse_map(&source, &config)?;
            dict.dump(&mut buf)?;
        }
        DictFormat::Set => {
            let dict = one_sixth::parser::parse_set(&source, &config)?;
            dict.dump(&mut buf)?;
        }
    }

    Ok(())
}
