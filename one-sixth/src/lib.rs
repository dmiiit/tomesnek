#[cfg(feature = "bin")]
pub mod parser;

use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
    io::{self, Write},
    ops::{BitAnd, BitOr, BitOrAssign},
};

use fst::{automaton::Str, IntoStreamer, Map, Set, Streamer};
use regex_automata::dense;

#[derive(Debug)]
pub enum Error {
    IoError { source: io::Error },
    InvalidDictionaryEntry { entry: String },
    InvalidInternalRepresentation { source: fst::Error },
    InvalidPattern { pattern: String, source: regex_automata::Error },
    InvalidWordFlags { value: u64 },
    MismatchedBracket { bracket: char, line: usize },
    SetTooLarge(u64),
    UnknownModifier(char),
}

impl Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::IoError { source } => write!(f, "IO error: {}", source),
            Self::InvalidDictionaryEntry { entry } => {
                write!(f, "unable to parse dictionary entry: {}", entry)
            }
            Self::InvalidInternalRepresentation { source } => {
                write!(f, "invalid serialized representation: {}", source)
            }
            Self::InvalidPattern { pattern, source } => {
                write!(f, "invalid search pattern {}: {}", pattern, source)
            }
            Self::InvalidWordFlags { value } => write!(f, "invalid word flags: 0x{:x}", value),
            Self::MismatchedBracket { bracket, line } => {
                write!(f, "mismatched {} on line {}", bracket, line + 1)
            }
            Self::SetTooLarge(size) => write!(f, "set is too large: {} bytes", size),
            Self::UnknownModifier(ch) => write!(f, "unknown word modifier: {}", ch),
        }
    }
}

impl std::error::Error for Error {}

impl From<io::Error> for Error {
    fn from(source: io::Error) -> Self { Self::IoError { source } }
}

impl From<fst::Error> for Error {
    fn from(source: fst::Error) -> Self { Self::InvalidInternalRepresentation { source } }
}

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Default, Debug, Clone, Copy, PartialEq, Eq)]
pub struct WordFlags(u16);

impl WordFlags {
    const NONE: Self = Self(0);

    // word types
    const ADJECTIVE_OR_ADVERB: Self = Self(1 << 0);
    const CONJUNCTION_OR_PREPOSITION: Self = Self(1 << 1);
    const INTERJECTION: Self = Self(1 << 2);
    const NOUN: Self = Self(1 << 3);
    const PRONOUN: Self = Self(1 << 4);
    const SPOKEN_CONTRACTION: Self = Self(1 << 5);
    const VERB: Self = Self(1 << 6);

    // word modifiers
    const ARCHAIC: Self = Self(1 << 7);
    const NEOLOGISM: Self = Self(1 << 8);
    const RARE: Self = Self(1 << 9);
    const UNCOUNTABLE_PLURAL: Self = Self(1 << 10);

    pub fn is_adjective_or_adverb(self) -> bool { self & Self::ADJECTIVE_OR_ADVERB != Self::NONE }
    pub fn is_conjunction(self) -> bool { self & Self::CONJUNCTION_OR_PREPOSITION != Self::NONE }
    pub fn is_interjection(self) -> bool { self & Self::INTERJECTION != Self::NONE }
    pub fn is_noun(self) -> bool { self & Self::NOUN != Self::NONE }
    pub fn is_pronoun(self) -> bool { self & Self::PRONOUN != Self::NONE }
    pub fn is_spoken_contraction(self) -> bool { self & Self::SPOKEN_CONTRACTION != Self::NONE }
    pub fn is_verb(self) -> bool { self & Self::VERB != Self::NONE }

    pub fn is_archaic(self) -> bool { self & Self::ARCHAIC != Self::NONE }
    pub fn is_neologism(self) -> bool { self & Self::NEOLOGISM != Self::NONE }
    pub fn is_rare(self) -> bool { self & Self::RARE != Self::NONE }
    pub fn is_uncountable_plural(self) -> bool { self & Self::UNCOUNTABLE_PLURAL != Self::NONE }
}

impl BitAnd for WordFlags {
    type Output = Self;

    fn bitand(self, rhs: Self) -> Self::Output { Self(self.0 & rhs.0) }
}

impl BitOr for WordFlags {
    type Output = WordFlags;

    fn bitor(self, rhs: Self) -> Self::Output { Self(self.0 | rhs.0) }
}

impl BitOrAssign for WordFlags {
    fn bitor_assign(&mut self, rhs: Self) { self.0 |= rhs.0; }
}

impl TryFrom<char> for WordFlags {
    type Error = Error;

    fn try_from(ch: char) -> Result<Self> {
        let flag = match ch {
            '@' => WordFlags::ARCHAIC,
            '+' => WordFlags::NEOLOGISM,
            '!' | '-' => WordFlags::RARE,
            '~' => WordFlags::UNCOUNTABLE_PLURAL,
            'A' => WordFlags::ADJECTIVE_OR_ADVERB,
            'C' => WordFlags::CONJUNCTION_OR_PREPOSITION,
            'I' => WordFlags::INTERJECTION,
            'N' => WordFlags::NOUN,
            'P' => WordFlags::PRONOUN,
            'S' => WordFlags::SPOKEN_CONTRACTION,
            'V' => WordFlags::VERB,
            _ => return Err(Error::UnknownModifier(ch)),
        };
        Ok(flag)
    }
}

impl TryFrom<u64> for WordFlags {
    type Error = Error;

    fn try_from(value: u64) -> Result<Self> {
        let value = value.try_into().map_err(|_| Error::InvalidWordFlags { value })?;
        Ok(WordFlags(value))
    }
}

#[derive(Debug, Clone)]
pub struct WordSetConfig {
    pub min_word_length: usize,
    pub include_archaic_words: bool,
    pub include_rare_words: bool,
    pub include_neologisms: bool,
    pub excluded_words: HashSet<String>,
}

impl Default for WordSetConfig {
    fn default() -> Self {
        Self {
            min_word_length: 3,
            include_archaic_words: true,
            include_rare_words: true,
            include_neologisms: true,
            excluded_words: Default::default(),
        }
    }
}

pub struct WordMap<B> {
    map: Map<B>,
}

impl<B> WordMap<B>
where
    B: AsRef<[u8]>,
{
    /// Read a [`Self`] from a binary representation.
    pub fn load(bytes: B) -> Result<Self> {
        let map = Map::new(bytes)?;
        Ok(Self { map })
    }

    /// Write a compact binary representation of the [`Self`].
    pub fn dump(&self, writer: &mut impl Write) -> Result<usize> {
        let bytes = self.map.as_fst().as_bytes();
        let size = bytes.len();
        writer.write_all(bytes)?;
        Ok(size)
    }

    /// Search for the exact word provided, and return its flags if a match is
    /// found.
    pub fn find_exact(&self, str: &str) -> Option<WordFlags> {
        let aut = Str::new(str);
        self.map
            .search(aut)
            .into_stream()
            .next()
            .map(|(_, flags)| flags.try_into().expect("word was found with invalid flags"))
    }

    /// Search for a regular expression, and return an iterator over all found
    /// matches along with their respective flags.
    pub fn find_pattern(&self, pat: &str) -> Result<impl Iterator<Item = (String, WordFlags)>> {
        let dfa = dense::Builder::new()
            .anchored(true)
            .build(pat)
            .and_then(|dfa| dfa.to_sparse())
            .map_err(|source| Error::InvalidPattern { pattern: pat.to_owned(), source })?;
        let iter = self.map.search(dfa).into_stream().into_str_vec().unwrap().into_iter().map(
            |(word, flags)| (word, flags.try_into().expect("word was found with invalid flags")),
        );
        Ok(iter)
    }
}

impl TryFrom<HashMap<&[u8], WordFlags>> for WordMap<Vec<u8>> {
    type Error = Error;

    fn try_from(words: HashMap<&[u8], WordFlags>) -> Result<Self> {
        let mut sorted = words.into_iter().map(|(w, f)| (w, u64::from(f.0))).collect::<Vec<_>>();
        sorted.sort_unstable();

        let map = Map::from_iter(sorted)?;
        Ok(WordMap { map })
    }
}

pub struct WordSet<B> {
    set: Set<B>,
}

impl<B> WordSet<B>
where
    B: AsRef<[u8]>,
{
    /// Read a [`Self`] from a binary representation.
    pub fn load(bytes: B) -> Result<Self> {
        let set = Set::new(bytes)?;
        Ok(Self { set })
    }

    /// Write a compact binary representation of the [`Self`].
    pub fn dump(&self, writer: &mut impl Write) -> Result<usize> {
        let bytes = self.set.as_fst().as_bytes();
        let size = bytes.len();
        writer.write_all(bytes)?;
        Ok(size)
    }

    /// Search for the exact word provided, and return `true` if a match is
    /// found.
    pub fn contains(&self, str: &str) -> bool { self.set.contains(str) }

    /// Search for a regular expression, and return an iterator over all found
    /// matches.
    pub fn find_pattern(&self, pat: &str) -> Result<impl Iterator<Item = String>> {
        let dfa = dense::Builder::new()
            .anchored(true)
            .build(pat)
            .and_then(|dfa| dfa.to_sparse())
            .map_err(|source| Error::InvalidPattern { pattern: pat.to_owned(), source })?;
        let iter = self.set.search(dfa).into_stream().into_strs().unwrap().into_iter();
        Ok(iter)
    }
}

impl TryFrom<HashMap<&[u8], WordFlags>> for WordSet<Vec<u8>> {
    type Error = Error;

    fn try_from(words: HashMap<&[u8], WordFlags>) -> Result<Self> {
        let mut sorted = words.into_keys().collect::<Vec<_>>();
        sorted.sort_unstable();

        let set = Set::from_iter(sorted)?;
        Ok(WordSet { set })
    }
}
