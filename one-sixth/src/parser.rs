use std::collections::HashMap;

use logos::Logos;

use crate::{Error, Result, WordFlags, WordMap, WordSet, WordSetConfig};

#[derive(Debug, PartialEq, Eq, Logos)]
pub enum Tok {
    // Modifiers.
    #[regex("[@~!+-]")]
    WordModifier,
    #[regex("[ACINPSV]")]
    WordType,

    // Structural.
    #[token(":")]
    Colon,
    #[token("{")]
    CurlyBracketL,
    #[token("}")]
    CurlyBracketR,
    #[token("\n")]
    Newline,
    #[token("(")]
    ParenL,
    #[token(")")]
    ParenR,
    #[token("/")]
    Slash,
    #[token(" ")]
    Space,
    #[token("|")]
    VerticalBar,

    // Word.
    #[regex("[a-z']+")]
    Word,

    // Error.
    #[error]
    Error,
}

#[cfg(test)]
mod tests {
    use logos::Logos;

    use super::Tok;

    #[test]
    fn test_lexer() {
        const SOURCE: &str = "wend V: wended (-@went)  wending  wends
wit A:";

        let mut lex = Tok::lexer(SOURCE);

        // Line #1.

        assert_eq!(lex.next(), Some(Tok::Word));
        assert_eq!(lex.span(), 0..4);
        assert_eq!(lex.slice(), "wend");

        assert_eq!(lex.next(), Some(Tok::Space));
        assert_eq!(lex.span(), 4..5);
        assert_eq!(lex.slice(), " ");

        assert_eq!(lex.next(), Some(Tok::WordType));
        assert_eq!(lex.span(), 5..6);
        assert_eq!(lex.slice(), "V");

        assert_eq!(lex.next(), Some(Tok::Colon));
        assert_eq!(lex.span(), 6..7);
        assert_eq!(lex.slice(), ":");

        assert_eq!(lex.next(), Some(Tok::Space));
        assert_eq!(lex.span(), 7..8);
        assert_eq!(lex.slice(), " ");

        assert_eq!(lex.next(), Some(Tok::Word));
        assert_eq!(lex.span(), 8..14);
        assert_eq!(lex.slice(), "wended");

        assert_eq!(lex.next(), Some(Tok::Space));
        assert_eq!(lex.span(), 14..15);
        assert_eq!(lex.slice(), " ");

        assert_eq!(lex.next(), Some(Tok::ParenL));
        assert_eq!(lex.span(), 15..16);
        assert_eq!(lex.slice(), "(");

        assert_eq!(lex.next(), Some(Tok::WordModifier));
        assert_eq!(lex.span(), 16..17);
        assert_eq!(lex.slice(), "-");

        assert_eq!(lex.next(), Some(Tok::WordModifier));
        assert_eq!(lex.span(), 17..18);
        assert_eq!(lex.slice(), "@");

        assert_eq!(lex.next(), Some(Tok::Word));
        assert_eq!(lex.span(), 18..22);
        assert_eq!(lex.slice(), "went");

        assert_eq!(lex.next(), Some(Tok::ParenR));
        assert_eq!(lex.span(), 22..23);
        assert_eq!(lex.slice(), ")");

        assert_eq!(lex.next(), Some(Tok::Space));
        assert_eq!(lex.span(), 23..24);
        assert_eq!(lex.slice(), " ");

        assert_eq!(lex.next(), Some(Tok::Space));
        assert_eq!(lex.span(), 24..25);
        assert_eq!(lex.slice(), " ");

        assert_eq!(lex.next(), Some(Tok::Word));
        assert_eq!(lex.span(), 25..32);
        assert_eq!(lex.slice(), "wending");

        assert_eq!(lex.next(), Some(Tok::Space));
        assert_eq!(lex.span(), 32..33);
        assert_eq!(lex.slice(), " ");

        assert_eq!(lex.next(), Some(Tok::Space));
        assert_eq!(lex.span(), 33..34);
        assert_eq!(lex.slice(), " ");

        assert_eq!(lex.next(), Some(Tok::Word));
        assert_eq!(lex.span(), 34..39);
        assert_eq!(lex.slice(), "wends");

        assert_eq!(lex.next(), Some(Tok::Newline));
        assert_eq!(lex.span(), 39..40);
        assert_eq!(lex.slice(), "\n");

        // Line #2.

        assert_eq!(lex.next(), Some(Tok::Word));
        assert_eq!(lex.span(), 40..43);
        assert_eq!(lex.slice(), "wit");

        assert_eq!(lex.next(), Some(Tok::Space));
        assert_eq!(lex.span(), 43..44);
        assert_eq!(lex.slice(), " ");

        assert_eq!(lex.next(), Some(Tok::WordType));
        assert_eq!(lex.span(), 44..45);
        assert_eq!(lex.slice(), "A");

        assert_eq!(lex.next(), Some(Tok::Colon));
        assert_eq!(lex.span(), 45..46);
        assert_eq!(lex.slice(), ":");

        assert_eq!(lex.next(), None);
    }
}

/// Parse a `HashMap` of word bytes to flags from a string in the `2of12id.txt`
/// format.
fn parse<'a>(source: &'a str, config: &WordSetConfig) -> Result<HashMap<&'a [u8], WordFlags>> {
    let mut words: HashMap<&[u8], WordFlags> = HashMap::new();
    let mut lex = Tok::lexer(source);

    // Flags that apply to all words on the current line.
    let mut line_number = 0usize;
    let mut line_flags = WordFlags::NONE;
    let mut word_flags = WordFlags::NONE;
    let mut first_word = true;
    while let Some(tok) = lex.next() {
        match tok {
            Tok::WordModifier => {
                let flag = WordFlags::try_from(lex.slice().chars().next().unwrap())?;
                if first_word {
                    line_flags |= flag;
                } else {
                    word_flags |= flag;
                }
            }
            Tok::WordType => {
                let flag = WordFlags::try_from(lex.slice().chars().next().unwrap())?;
                line_flags |= flag;
            }
            Tok::Colon => {
                first_word = false;
            }
            Tok::Newline => {
                line_number += 1;
                line_flags = WordFlags::NONE;
                first_word = true;
            }
            Tok::CurlyBracketL => {
                // Skip everything until the closing curly bracket.
                for tok in lex.by_ref() {
                    if tok == Tok::CurlyBracketR {
                        break;
                    }
                }
            }
            Tok::CurlyBracketR => {
                // Consumed by the handler for {.
                return Err(Error::MismatchedBracket {
                    bracket: lex.slice().chars().next().unwrap(),
                    line: line_number,
                });
            }
            Tok::ParenL | Tok::ParenR | Tok::Slash | Tok::VerticalBar | Tok::Space => { /* do nothing */
            }
            Tok::Word => {
                let word = lex.slice();
                //TODO: skip words based on the excluded modifiers from the config
                if word.len() < config.min_word_length
                    || word.contains('\'')
                    || config.excluded_words.contains(word)
                {
                    // skip
                    continue;
                }

                let flags = line_flags | word_flags;
                words.insert(lex.slice().as_bytes(), flags);
                word_flags = WordFlags::NONE;
            }
            Tok::Error => {
                return Err(Error::InvalidDictionaryEntry { entry: lex.slice().to_owned() });
            }
        }
    }

    Ok(words)
}

pub fn parse_map(source: &str, config: &WordSetConfig) -> Result<WordMap<Vec<u8>>> {
    parse(source, config)?.try_into()
}

pub fn parse_set(source: &str, config: &WordSetConfig) -> Result<WordSet<Vec<u8>>> {
    parse(source, config)?.try_into()
}
